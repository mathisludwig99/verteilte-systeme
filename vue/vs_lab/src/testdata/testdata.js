export function getData() {
  let text =
    '{ "todos" : [' +
    '{ "name":"Bachelorarbeit abgeben", "priority":1 },' +
    '{ "name":"VS Labor fertigstellen", "priority":2  },' +
    '{ "name":"Präsentation vorbereiten", "priority":3  },' +
    '{ "name":"Abschluss feiern", "priority":4  } ]}';
  return JSON.parse(text);
}
