import axios from "axios";

export async function getItems() {
    const items = axios.get("http://localhost:9000/todos/", {});
    return items;
}

export async function getItem(name) {
    const items = axios.get("http://localhost:9000/todos/"+name, {});
    return items;
}

export async function newItem(todo, priority) {
    const items = axios.post("/todos/", {
        todo: todo,
        priority: priority
    });
    return items;
}

export async function deleteItem(name) {
    const items = axios.delete("http://localhost:9000/todos/"+name);
    return items;
}

export async function changeItem(name, newPriority) {
    const items = axios.put("http://localhost:9000/todos/", {name: name, newPriority: newPriority});
    return items;
}