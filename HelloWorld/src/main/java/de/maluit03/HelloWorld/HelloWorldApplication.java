package de.maluit03.HelloWorld;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class HelloWorldApplication {

	private Logger logger = LoggerFactory.getLogger(HelloWorldApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(HelloWorldApplication.class, args);
	}

	@RestController
	class Rest {
		@Autowired
		private TodoRepository todoRepository;
		private String testString = "User";

		// Simple Requests
		@GetMapping("/")
		String get() {
			logger.info("Seite besucht");
			return "Hello " + testString;
		}

		@GetMapping("/{echo}")
		String echo(@PathVariable String echo) {
			return echo;
		}

		@PostMapping("/{name}")
		void post(@PathVariable String name) {
			logger.info("Name set to {}", name);
			testString = name;
		}

		// InMemory DB Requests
		@GetMapping("/todo")
		ResponseEntity<List<Todo>> getTodos() {
			try {
				List<Todo> list = todoRepository.findAll();
				if (list.isEmpty() || list.size() == 0) {
					return new ResponseEntity<List<Todo>>(list, HttpStatus.NO_CONTENT);
				}

				return new ResponseEntity<List<Todo>>(list, HttpStatus.OK);
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@PostMapping("/todo/add/")
		public ResponseEntity<Todo> createTodo(@RequestBody Todo todo) {
			try {
				return new ResponseEntity<Todo>(todoRepository.save(todo), HttpStatus.CREATED);
			} catch (Exception exception) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@GetMapping("/todo/{id}")
		public ResponseEntity<Todo> getTodoById(@PathVariable Long id) {
			try {

				var todo = todoRepository.findById(id);
				if (todo.isPresent()) {
					return ResponseEntity.notFound().build();
				}
				return ResponseEntity.ok(todo.get());
			} catch (Exception e) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@DeleteMapping("/todo/{id}")
		public ResponseEntity<Void> deleteTodo(@PathVariable Long id) {
			try {
				todoRepository.deleteById(id);
				return new ResponseEntity<Void>(HttpStatus.OK);
			} catch (Exception exception) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}

		@PutMapping("/todo/{id}")
		public ResponseEntity<Todo> updateTodo(@RequestBody Todo todo) {
			try {
				return new ResponseEntity<Todo>(todoRepository.save(todo), HttpStatus.OK);
			} catch (Exception exception) {
				return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
}
